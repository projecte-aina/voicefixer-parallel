# Voicefixer parallel
Multiprocessing tests for voicefixer.

## Usage
In order to install just
```
pip install voicefixer
```

And to launch the test task, first create a `test_out` directory, and then:

```
./voicefixer.py -ifdr test_in/ -ofdr test_out/ -w 3
```

This launches the process with 3 workers
